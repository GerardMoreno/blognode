var express = require("express"),
    handlebars = require('express-handlebars');
 
 
var bodyParser = require('body-parser');

 
var app = express();
 
 
var posts = [{
                "subject": "Post number 1",
                "description": "Description 1",
                "time": new Date()
            },
            {
                "subject": "Post number 2",
                "description": "Description 2",
                "time": new Date()
            }
            ];
 
 
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));
// parse application/json
app.use(bodyParser.json());     
app.engine('handlebars', handlebars());
app.set('view engine', 'handlebars');
app.set("views", "./views");
 
 //ver todos los posts
app.get('/posts', function(req, res){
	
    res.render('posts', { "title": "Hola gente", "posts" : posts } );
});

//ver ultimo post
app.get("/posts/new", function(req, res) {
	
	var post = posts[posts.length-1];
	res.render('posts', { "title": "Last post ", "post": {post}  } );
});

//coger el tipo de post q usaremos
function getPostObj(req){
	
	var post ={"subject" : req.query.subject,
				"description": req.query.description,
				 "time": new Date()
				};
	return post;
}
 
 //postear objeto
app.post('/posts', function(req, res){
	
	posts.push(getPostObj(req));
});

//editar ultimo post
app.get("/posts/edit", function(req, res) {
	
	posts[posts.length-1] = getPostObj(req);
	res.render('posts', { "title": "Post edited successfully"} );
});

//ver el post por id
app.get("/posts/:id", function(req, res) {
	var post = posts[req.params.id];
	if(req.params.id < posts.length){
	 	res.render('posts', { "title": "Post ID ", "post": {post}  } );
	}
	else{
		res.render('posts', { "title": "The post was not found"} );
	}
});

//añadir un post a la array de posts
app.put("/posts/:id", function(req, res) {
	
	if(req.params.id < posts.length){
		posts[req.params.id] = getPostObj(req);
	 	res.render('posts', { "title": "The post was found"} );
	}
	else{
		res.render('posts', { "title": "The post was not found"} );
	}
});

//borrar post por id
app.delete("/posts/:id", function(req, res) {
	
	if(req.params.id < posts.length){
		posts.splice(req.params.id,1);
	 	res.render('posts', { "title": "Post deleted successfully"} );
	}
	else{
		res.render('posts', { "title": "The post was not found"} );
	}
});
 
app.listen(8080);